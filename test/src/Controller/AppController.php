<?php
namespace App\Controller;


use App\Form\ProductType;
use App\Form\CustomerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AppController extends AbstractController
{

    public function getAllValue($values)
    {
        return $this->getDoctrine()->getRepository($values)->findAll();

    }


    public function formSubmittedValid($values, $form)
    {

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($values);
            $entityManager->flush();
            return true;
        }
        return  false;
    }

    public function formCustomerCreateHandle($valus, $request)
    {
        return $this->createForm(CustomerType::class, $valus)->handleRequest($request);
    }

    public function formProductCreateHandle($valus, $request)
    {
        return $this->createForm(ProductType::class, $valus)->handleRequest($request);
    }


}


