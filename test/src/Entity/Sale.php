<?php

namespace App\Entity;

use DateTimeInterface;
use App\Entity\SaleProduct;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Sale
 *
 * @ORM\Table(name="sale")
 * @ORM\Entity(repositoryClass="App\Repository\SaleRepository")
 * 
 */
class Sale
{//@ORM\OrderBy({"amount_sales" = "ASC"})
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date_of_purchase", type="datetime_immutable", nullable=false)
     */
    private $dateOfPurchase;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="sales")
     * @ORM\JoinColumn(nullable=true)
     */
    private $customer;

    /**
     * @var float|null
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=true)
     */
    private $amount;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SaleProduct", mappedBy="sale")
     */
    private $saleProducts;

    public function __construct()
    {
        $this->saleProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateOfPurchase(): ?\DateTimeInterface
    {
        return $this->dateOfPurchase;
    }

    public function setDateOfPurchase(string $dateOfPurchase): self
    {
        $this->dateOfPurchase = $dateOfPurchase;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|SaleProduct[]
     */
    public function getSaleProducts(): Collection
    {
        return $this->saleProducts;
    }

    public function addSaleProduct(SaleProduct $saleProduct): self
    {
        if (!$this->saleProducts->contains($saleProduct)) {
            $this->saleProducts[] = $saleProduct;
            $saleProduct->setSale($this);
        }

        return $this;
    }

    public function removeSaleProduct(SaleProduct $saleProduct): self
    {
        if ($this->saleProducts->contains($saleProduct)) {
            $this->saleProducts->removeElement($saleProduct);
            // set the owning side to null (unless already changed)
            if ($saleProduct->getSale() === $this) {
                $saleProduct->setSale(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return sprintf( "%d", $this->getDateOfPurchase());
    }


}
