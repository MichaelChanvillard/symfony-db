<?php
namespace App\Controller;

use App\Controller\AppController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminLog extends AbstractController

{

/**
* @Route("/admin/log", name="adminLog")
* @return Response
 * @param Request $request
*/
public function index(Request $request) : Response
{
return $this->render('Admin/adminLog.html.twig',
    [
        'current_menu' => 'adminLog'
    ]);
}

}