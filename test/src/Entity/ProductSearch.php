<?php
namespace App\Entity;



class ProductSearch
{
    /**
     * @var int
     */
    private $maxPrice;

    /**
     * @var int
     */
    private $minPrice;

    /**
     * @param int|null $minPrice
     * @return ProductSearch
     */
    public function setMinPrice(?int $minPrice): ProductSearch
    {
        $this->minPrice = $minPrice;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;

    }
    /**
     * @param int|null $maxPrice
     * @return ProductSearch
     */
    public function setMaxPrice(?int $maxPrice): ProductSearch
    {
        $this->maxPrice = $maxPrice;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinPrice(): ?int
    {
        return $this->minPrice;
    }

}


