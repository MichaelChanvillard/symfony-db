<?php
namespace App\Controller;

use App\Controller\AppController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminIndex extends AbstractController
{
    /**
     * @Route("/admin/home", name="adminIndex")
     * @return Response
     */
    public function index(Request $request) : Response
    {
        return $this->render('Admin/adminIndex.html.twig',
            [
                'current_menu' => 'adminLog'
            ]);
    }
}