<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Sale;
use App\Entity\SaleProduct;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SaleProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantityProduct')
            ->add('color')
            ->add('product', EntityType::class,[
                'class' => Product::class,
                'choice_label' => 'name',
                'multiple' => true,

            ])
            ->add('sale', SaleType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SaleProduct::class,
        ]);
    }
}

