<?php
namespace App\Controller;


use App\Controller\AppController;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopProduct extends AbstractController
{
    private $appController;

    public function  __construct(AppController $appController)
    {
        $this->appController = $appController;
    }
    /**
     * @Route("/Produits", name="shopProduct")
     */
    public function index() : Response
    {
        $products = Product::class;

        return $this->render('User/user.html.twig',
            [
                'products' => $this->appController->getAllValue($products),
                'current_menu' => 'shopProduct'
            ]);
    }
}